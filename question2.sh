# bin bash

head -n 5000 opra_example_regression.log > small.log1 # get a small sample file
awk '{$1=""; print $0}' small.log1 > small.log2 # remove regression
# find needed record
awk '/Record Publish|Type: Trade|wTradePrice|wTradeVolume' small.log2 > small.log3
grep -A 2 -B 1 'Tpye: Trade' small.log4 > small.log5 #get the record of a trade
cat small.log5 | sed 'N;s/\n T/, t/' # change the format display the file