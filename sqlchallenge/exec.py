import pymysql, csv

#con = sqlite3.connect('C:\\Users\Administrator\PycharmProjects\sqlchallenge\trade.db')

con = pymysql.connect(host = 'localhost',
                      user = 'root',
                      passwd ='c0nygre',
                      db = 'trade.db')
cur = con.cursor()

#cur.execute("drop table trade_date")
#cur.execute("CREATE TABLE trade_data (ticker, date, open, high, low, close, vol)")

with open('C:\Users\Administrator\Downloads\sample_dataset2.csv', 'r') as fin:
     dr = csv.DictReader(fin)
     dicts = [(i['ticker'], i['date']
               ,i['open'], i['high'], i['low'], i['close'], i['vol'])
               for i in dr]
cur.execute("INSERT INTO trade_date (ticker, date, open, high, low, close, vol) \
values (?, ?, ?, ?, ?, ?, ?);", dicts)


con.commit()
con.close()

cur.execute("select * from trade_date")
ans = cur.fetchall()

for i in ans:
    print (i)
